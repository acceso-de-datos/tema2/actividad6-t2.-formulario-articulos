package vista;

import com.opencsv.CSVReader;
import controladorbd.ConexionBD;
import dao.ArticuloDAO;

import dao.ClienteDAO;
import dao.GrupoDAO;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import modelo.Articulo;
import modelo.Grupo;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;


public class ControllerArticulo implements Initializable {
    public static final String SEPARATOR = ",";
    @FXML
    private TableView<Articulo> tvContenido;

    @FXML
    private TableColumn<Articulo, String> id;

    @FXML
    private TableColumn<Articulo, String> Nombre;

    @FXML
    private TableColumn<Articulo, String> Precio;

    @FXML
    private TableColumn<Articulo, String> Codigo;

    @FXML
    private TableColumn<Articulo, String> Grupo;

    @FXML
    private TextField tfnom;

    @FXML
    private TextField tfprecio;

    @FXML
    private TextField tfcodigo;

    @FXML
    private ComboBox<Grupo> cbgrupo;

    @FXML
    private TableView<Grupo> tvGrup;

    @FXML
    private TableColumn<Grupo, String> grId;

    @FXML
    private TableColumn<Grupo, String> grDescripcion;

    @FXML
    private TextField descripcion;


    private ArticuloDAO artDAO;
    private GrupoDAO grpDAO;
    private List<Articulo> ListArticulo;
    private List<Grupo> ListGrupo;

    private List<String[]> ListCsv;

    private int[] notificaciones ;

    int  liiinea;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        int  liiinea=0;
        notificaciones= new int[0];

        //PARA INSERTAR GRUPOS
        grId.setCellValueFactory(new PropertyValueFactory<Grupo, String>("Id"));
        grDescripcion.setCellValueFactory(new PropertyValueFactory<Grupo, String>("Descripcion"));

        //PARA INSERTAR aRTICULOS VINCULANDO LAS COLUMNAS
        id.setCellValueFactory(new PropertyValueFactory<Articulo, String>("Id"));
        Nombre.setCellValueFactory(new PropertyValueFactory<Articulo, String>("Nombre"));
        Precio.setCellValueFactory(new PropertyValueFactory<Articulo, String>("Precio"));
        Codigo.setCellValueFactory(new PropertyValueFactory<Articulo, String>("Codigo"));
        Grupo.setCellValueFactory(new PropertyValueFactory<Articulo, String>("Grupo"));

        try {
            grpDAO = new GrupoDAO();
            artDAO = new ArticuloDAO();
            ListArticulo = artDAO.findAll();
            ListGrupo = grpDAO.findAll();



            ObservableList<Articulo> lists = FXCollections.observableArrayList(ListArticulo);
            ObservableList<Grupo> listGrupo = FXCollections.observableArrayList(ListGrupo);

            //metemos todos los grupos el tableview
            tvGrup.setItems(listGrupo);

            //metemos lista de grupo en combobox
            cbgrupo.setItems(listGrupo);

//            Metemos los datos a la tabla
            tvContenido.setItems(lists);
            tvContenido.setOnMouseClicked(new EventHandler<Event>() {


                public void handle(Event event) {
                    //cogemos el seleccionado y lo metemos en los textfield

                    tfnom.setText(tvContenido.getSelectionModel().getSelectedItems().get(0).getNombre());
                    tfprecio.setText(String.valueOf(tvContenido.getSelectionModel().getSelectedItems().get(0).getPrecio()));
                    tfcodigo.setText(String.valueOf(tvContenido.getSelectionModel().getSelectedItems().get(0).getCodigo()));

                    Grupo grupo = (Grupo) tvContenido.getSelectionModel().getSelectedItems().get(0).getGrupo();
                    cbgrupo.setValue(grupo);
                }

            });

        } catch (SQLException ex) {
            Util.mensajeExcepcion(ex, "Conectando con la base de datos...");
            Platform.exit();
        }
    }

    @FXML
    void borrar(ActionEvent event) throws SQLException {
        Articulo art;
        art = tvContenido.getSelectionModel().getSelectedItem();
        if (artDAO.delete(art)) {

            ListArticulo.remove(art);
            tfnom.setText("");
            tfcodigo.setText("");
            tfprecio.setText("");
            mostrarDatos();
        }
    }

    @FXML
    void importar(ActionEvent event) throws Exception {
        int linea=0;

        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "CSV", "csv");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {


            leerFichero(chooser.getSelectedFile().getAbsolutePath());
            notificaciones = artDAO.ejecutarbatch();




        }
    }

    @FXML
    void modificar(ActionEvent event) throws SQLException {
        Articulo art;
        art = new Articulo(tvContenido.getSelectionModel().getSelectedItem().getId(), tfnom.getText(), Float.parseFloat(tfprecio.getText()), tfprecio.getText(), cbgrupo.getSelectionModel().getSelectedItem());
        if (artDAO.update(art)) {

            for (Articulo ar : ListArticulo) {
                if (ar.getId() == art.getId()) {
                    ListArticulo.set(ListArticulo.indexOf(ar), art);
                    continue;
                }
            }
            mostrarDatos();
        }


    }

    @FXML
    void nuevo(ActionEvent event) throws SQLException {

        Articulo art;
        art = new Articulo(tfnom.getText(), Float.parseFloat(tfprecio.getText()), tfprecio.getText(), cbgrupo.getSelectionModel().getSelectedItem());
        art = artDAO.insertGenKey(art);
        ListArticulo.add(art);
        mostrarDatos();

    }

    public void mostrarDatos() {

        ObservableList<Articulo> lists = FXCollections.observableArrayList(ListArticulo);
        tvContenido.setItems(lists);
    }
    public void mostrarGrupos() {

        ObservableList<Grupo> lists = FXCollections.observableArrayList(ListGrupo);
        tvGrup.setItems(lists);
    }

    public void leerFichero(String ruta) throws Exception {
        GrupoDAO gdao = new GrupoDAO();
        int  liiinea=0;
        // String[] campos;
        BufferedReader bufferLectura = null;

        try {
            //LEER ARCHIVO Y INSERTAR LOTE Y ACABAR FOR ejecutarbatch


            // Abrir el .csv en buffer de lectura
            bufferLectura = new BufferedReader(new FileReader(ruta));

            // Leer una linea del archivo
            String linea = bufferLectura.readLine();

            while (linea != null) {
                //String campos = linea;

                String[] campos = linea.split(SEPARATOR);

                //System.out.println(Arrays.toString(campos));
                if (isNumeric(campos[4]) && isString(campos[1]) && isString(campos[2]) && isNumeric(campos[3])) {

                    Grupo g = gdao.findByPK(Integer.parseInt(campos[3]));
                    //System.out.println(liiinea+" Lineaa "+campos[3]+" "+g);
                    if(g!=null) {
                        artDAO.insertLote(new Articulo(campos[1], Float.parseFloat(campos[4]), campos[2], g));

                    }else{
                        for (int i=0; i<notificaciones.length-1;i++){
                            notificaciones[i]=-3;
                        }
                    }

                }else{
                    for (int i=0; i<notificaciones.length-1;i++){
                        notificaciones[i]=-3;
                    }
                }
                // Volver a leer otra línea del fichero
                linea = bufferLectura.readLine();
                liiinea++;
            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Cierro el buffer de lectura
            if (bufferLectura != null) {
                try {
                    bufferLectura.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    @FXML
    void insertGrup(ActionEvent event) throws SQLException {
        if(descripcion.getText()!=""){
            Grupo g = new Grupo(descripcion.getText());
           g=grpDAO.insertGenKey(g);
           if (g!=null){
            ListGrupo.add(g);
               mostrarGrupos();
           }

        }
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Float.parseFloat(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public boolean isString(Object str) {
        if (str.equals(str.toString())) {
            return true;
        } else {
            return false;
        }

    }

}
