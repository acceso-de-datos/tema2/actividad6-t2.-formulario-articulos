package dao;

import modelo.Grupo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GrupoDAO implements GenericDAO<Grupo> {
	final String SQLSELECTALL = "{ CALL empresa_ad.findall_grupo() }";
	final String SQLSELECTPK = "{ CALL empresa_ad.findbypk_grupo(?) }";
	final String SQLINSERT = "{ CALL empresa_ad.insert_grupo(?,?) }";
	final String SQLUPDATE = "{ CALL empresa_ad.update_grupo(?,?) }";
	final String SQLDELETE = "{ CALL empresa_ad.delete_grupo(?) }";
	private final CallableStatement pstSelectPK;
	private final CallableStatement pstSelectAll;
	private PreparedStatement pstSelect ;
	private final CallableStatement pstInsert;
	private final CallableStatement pstInsertGenKey;
	private final CallableStatement pstUpdate;
	private final CallableStatement pstDelete;
	
	public GrupoDAO() throws SQLException {
		Connection con = controladorbd.ConexionBD.getConexion();
		pstSelectPK = con.prepareCall(SQLSELECTPK);
		pstSelectAll = con.prepareCall(SQLSELECTALL);
		pstInsert = con.prepareCall(SQLINSERT);
		pstInsertGenKey =con.prepareCall(SQLINSERT);
		pstUpdate = con.prepareCall(SQLUPDATE);
		pstDelete = con.prepareCall(SQLDELETE);
	}
	public void cerrar() throws SQLException {
		pstSelectPK.close();
		pstSelectAll.close();
		pstInsert.close();
		pstUpdate.close();
		pstDelete.close();
	}

	@Override
	public Grupo findByPK(int id) throws SQLException {
		Grupo gr = null;

		pstSelectPK.setInt(1, id);
		ResultSet rs = pstSelectPK.executeQuery();
		if (rs.first()) {
			return new Grupo(id,rs.getString("descripcion"));
		}
		rs.close();
		return gr;
	
	}

	@Override
	public List<Grupo> findAll() throws SQLException {
		List<Grupo> listaGrupo = new ArrayList<Grupo>();
		ResultSet rs = pstSelectAll.executeQuery();
		while (rs.next()) {
			listaGrupo.add(new Grupo(rs.getInt("id"),rs.getString("descripcion")));
		}
		rs.close();
		return listaGrupo;
	}

	@Override
	public List<Grupo> findByExample(Grupo grup) throws SQLException {
		String sql = "";
		Connection con = controladorbd.ConexionBD.getConexion();
		List<Grupo>listaGrupos = new ArrayList<Grupo>();
		if(grup.getDescripcion()!=null){
			sql="SELECT * FROM grupos WHERE descripcion like '%"+grup.getDescripcion()+"%'";
		}

		pstSelect=con.prepareStatement(sql);
		ResultSet rs = pstSelect.executeQuery();
		if (rs.first()) {
			listaGrupos.add(new Grupo(rs.getInt("id"), rs.getString("descripcion")));
		}

		return listaGrupos;
	}

	@Override
	public boolean insert(Grupo grup) throws SQLException {
		pstInsert.setString(1, grup.getDescripcion());
		pstInsert.registerOutParameter(2, Types.INTEGER);
		int insertados = pstInsert.executeUpdate();
		return (insertados == 1);
	}

	@Override
	public Grupo insertGenKey(Grupo grup) throws SQLException {
		Grupo gr = null;;
		pstInsertGenKey.setString(1, grup.getDescripcion());
		pstInsertGenKey.registerOutParameter(2, Types.INTEGER);
		int insertados = pstInsertGenKey.executeUpdate();
		if (insertados==1){


			gr= new Grupo(pstInsertGenKey.getInt(2),grup.getDescripcion());
			return gr;
		}
		return gr ;
	}

	@Override
	public boolean update(Grupo grup) throws SQLException {
		pstUpdate.setString(1, grup.getDescripcion());
		pstUpdate.setInt(2, grup.getId());
		int insertados = pstInsert.executeUpdate();
		return (insertados == 1);
	}

	@Override
	public boolean delete(int id) throws SQLException {
		pstDelete.setInt(1, id);
		int borrados = pstDelete.executeUpdate();
		return (borrados == 1);
	}

	@Override
	public boolean delete(Grupo grup) throws SQLException {
		return this.delete(grup.getId());
		
	}

}
