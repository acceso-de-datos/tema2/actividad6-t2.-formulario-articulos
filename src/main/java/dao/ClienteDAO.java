package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import modelo.Cliente;

/**
 * @author sergio
 */
public class ClienteDAO implements GenericDAO<Cliente> {

    final String SQLSELECTALL = "SELECT * FROM clientes";
    final String SQLFINDBYEXAMPLE = "SELECT * FROM clientes WHERE nombre like ? and direccion like ?";

    final String SQLSELECTPK = "SELECT * FROM clientes WHERE id = ?";
    final String SQLINSERT = "INSERT INTO clientes (nombre, direccion) VALUES (?, ?)";
    final String SQLUPDATE = "UPDATE clientes SET nombre = ?, direccion = ? WHERE id = ?";
    final String SQLDELETE = "DELETE FROM clientes WHERE id = ?";
    private final PreparedStatement pstSelectPK;
    private final PreparedStatement pstSelectAll;
    private final PreparedStatement pstSelectByExample;

    private final PreparedStatement pstInsert;
    private final PreparedStatement pstInsertGenKey;
    private final PreparedStatement pstUpdate;
    private final PreparedStatement pstDelete;

    public ClienteDAO() throws SQLException {
        Connection con = controladorbd.ConexionBD.getConexion();
        pstSelectPK = con.prepareStatement(SQLSELECTPK);
        pstSelectAll = con.prepareStatement(SQLSELECTALL);
        pstSelectByExample = con.prepareStatement(SQLFINDBYEXAMPLE);
        pstInsert = con.prepareStatement(SQLINSERT);
        pstInsertGenKey = con.prepareStatement(SQLINSERT, Statement.RETURN_GENERATED_KEYS);
        pstUpdate = con.prepareStatement(SQLUPDATE);
        pstDelete = con.prepareStatement(SQLDELETE);

    }

    public void cerrar() throws SQLException {
        pstSelectPK.close();
        pstSelectAll.close();
        pstSelectByExample.close();
        pstInsert.close();
        pstUpdate.close();
        pstDelete.close();
    }

    public Cliente findByPK(int id) throws SQLException {
    	
        Cliente c = null;
        pstSelectPK.setInt(1, id);
        ResultSet rs = pstSelectPK.executeQuery();
        if (rs.first()) {
            return new Cliente(id, rs.getString("nombre"), rs.getString("direccion"));
        }
        rs.close();
        return c;
    }

    public List<Cliente> findAll() throws SQLException {
        List<Cliente> listaClientes = new ArrayList<Cliente>();
        ResultSet rs = pstSelectAll.executeQuery();
        while (rs.next()) {
            listaClientes.add(new Cliente(rs.getInt("id"), rs.getString("nombre"), rs.getString("direccion")));
        }
        rs.close();
        return listaClientes;
    }

    @Override
    public List<Cliente> findByExample(Cliente cli) throws SQLException {


        List<Cliente> listaCliente = new ArrayList<Cliente>();

        if (cli.getNombre() != null) {

            this.pstSelectByExample.setString(1, "%" + cli.getNombre() + "%");

        } else {
            this.pstSelectByExample.setString(1, "%");
        }
        if (cli.getDireccion() != null) {
            this.pstSelectByExample.setString(2, "%" + cli.getDireccion() + "%");

        } else {
            this.pstSelectByExample.setString(2, "%");
        }
        ;

        ResultSet rs = pstSelectByExample.executeQuery();
        if (rs.first()) {
            listaCliente.add(new Cliente(rs.getInt("id"), rs.getString("nombre"), rs.getString("direccion")));
        }

        return listaCliente;
    }

    public boolean insert(Cliente cliInsertar) throws SQLException {
        pstInsert.setString(1, cliInsertar.getNombre());
        pstInsert.setString(2, cliInsertar.getDireccion());
        int insertados = pstInsert.executeUpdate();
        return (insertados == 1);
    }

    @Override
    public Cliente insertGenKey(Cliente cliInsertar) throws SQLException {
        Cliente cli = null;
        pstInsertGenKey.setString(1, cliInsertar.getNombre());
        pstInsertGenKey.setString(2, cliInsertar.getDireccion());
        int insertados = pstInsertGenKey.executeUpdate();
        if (insertados == 1) {
            ResultSet rsClaves = pstInsertGenKey.getGeneratedKeys();
            rsClaves.first();

            cli = new Cliente(rsClaves.getInt(1), cliInsertar.getNombre(), cliInsertar.getDireccion());
            return cli;
        }
        return cli;
    }

    public boolean update(Cliente cliActualizar) throws SQLException {
        pstUpdate.setString(1, cliActualizar.getNombre());
        pstUpdate.setString(2, cliActualizar.getDireccion());
        pstUpdate.setInt(3, cliActualizar.getId());
        int actualizados = pstUpdate.executeUpdate();

        return (actualizados == 1);
    }

    public boolean delete(int id) throws SQLException {
        pstDelete.setInt(1, id);
        int borrados = pstDelete.executeUpdate();
        return (borrados == 1);
    }

    public boolean delete(Cliente cliEliminar) throws SQLException {
        return this.delete(cliEliminar.getId());
    }

}
