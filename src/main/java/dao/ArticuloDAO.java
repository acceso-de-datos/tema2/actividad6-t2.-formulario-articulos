package dao;

import javafx.scene.control.Alert;
import modelo.Articulo;
import modelo.Grupo;
import vista.Util;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArticuloDAO implements GenericDAO<Articulo> {

    final String SQLSELECTALL = "{ CALL empresa_ad.findall_articulo() }";
    final String SQLSELECTPK = "{ CALL empresa_ad.findbypk_articulo(?) }";
    final String SQLINSERT = "{ CALL empresa_ad.insert_articulo(?,?,?,?,?) }";
    final String SQLINSET = "INSERT INTO articulos (nombre, precio, codigo, grupo) VALUES (?, ?, ?, ?)";
    final String SQLFINDBYEXAMPLE = "SELECT * FROM articulos WHERE nombre like ? AND  precio like ? AND  codigo like ? AND grupo like ?";
    final String SQLUPDATE = "{ CALL empresa_ad.update_articulo(?,?,?,?,?) }";
    final String SQLDELETE = "{ CALL empresa_ad.delete_articulo(?) }";
    private final CallableStatement pstSelectPK;
    private final CallableStatement pstSelectAll;
    private PreparedStatement pstSelectByExample;
    private PreparedStatement pstInst;
    private final CallableStatement pstInsert;
    private final CallableStatement pstInsertGenKey;
    private final CallableStatement pstUpdate;
    private final CallableStatement pstDelete;
    private Connection con;


    public ArticuloDAO() throws SQLException {
        con = controladorbd.ConexionBD.getConexion();
        pstSelectPK = con.prepareCall(SQLSELECTPK);
        pstSelectAll = con.prepareCall(SQLSELECTALL);
        pstSelectByExample = con.prepareStatement(SQLFINDBYEXAMPLE);
        pstInsert = con.prepareCall(SQLINSERT);
        pstInst = con.prepareStatement(SQLINSET);
        pstInsertGenKey = con.prepareCall(SQLINSERT);
        pstUpdate = con.prepareCall(SQLUPDATE);
        pstDelete = con.prepareCall(SQLDELETE);
    }

    public void cerrar() throws SQLException {
        pstSelectPK.close();
        pstSelectAll.close();
        pstSelectByExample.close();
        pstInsert.close();
        pstUpdate.close();
        pstDelete.close();
    }

    @Override
    public Articulo findByPK(int id) throws SQLException {
        Articulo a = null;
        GrupoDAO grDAO = new GrupoDAO();
        pstSelectPK.setInt(1, id);
        ResultSet rs = pstSelectPK.executeQuery();
        if (rs.first()) {
            return new Articulo(id, rs.getString("nombre"), rs.getFloat("precio"), rs.getString("codigo"), grDAO.findByPK(rs.getInt("grupo")));
        }
        rs.close();
        return a;
    }

    @Override
    public List<Articulo> findAll() throws SQLException {
        GrupoDAO grDAO = new GrupoDAO();
        List<Articulo> listaArticulos = new ArrayList<Articulo>();
        ResultSet rs = pstSelectAll.executeQuery();
        while (rs.next()) {
            listaArticulos.add(new Articulo(rs.getInt("id"), rs.getString("nombre"), rs.getFloat("precio"), rs.getString("codigo"), grDAO.findByPK(rs.getInt("grupo"))));
        }
        rs.close();
        return listaArticulos;
    }

    public List<Articulo> findArticulosOfGrupo(Grupo grup) throws SQLException {
        GrupoDAO grDAO = new GrupoDAO();
        List<Articulo> listaArticulos = new ArrayList<Articulo>();
        this.pstSelectByExample.setString(1, "%");
        this.pstSelectByExample.setString(2, "%");
        this.pstSelectByExample.setString(3, "%");
        this.pstSelectByExample.setString(4, "%" + grup.getId() + "%");
        ResultSet rs = pstSelectByExample.executeQuery();
        while (rs.next()) {
            listaArticulos.add(new Articulo(rs.getInt("id"), rs.getString("nombre"), rs.getFloat("precio"), rs.getString("codigo"), grDAO.findByPK(rs.getInt("grupo"))));
        }
        rs.close();
        return listaArticulos;
    }

    @Override
    public List<Articulo> findByExample(Articulo arti) throws SQLException {
        // nombre like '%?%' AND  precio like '%?%' AND  codigo like '%?%' AND grupo like '%?%'
        GrupoDAO grDAO = new GrupoDAO();
        List<Articulo> listaArticulos = new ArrayList<Articulo>();


        if (arti.getNombre() != null) {

            this.pstSelectByExample.setString(1, "%" + arti.getNombre() + "%");

        } else {
            this.pstSelectByExample.setString(1, "%");
        }
        if (arti.getPrecio() > 0) {
            this.pstSelectByExample.setFloat(2, arti.getPrecio());

        } else {
            this.pstSelectByExample.setString(2, "%");

        }
        if (arti.getCodigo() != null) {
            this.pstSelectByExample.setString(3, arti.getCodigo());

        } else {
            this.pstSelectByExample.setString(3, "%");

        }
        if (arti.getGrupo() != null) {
            this.pstSelectByExample.setString(4, "%" + arti.getGrupo().getId() + "%");
        } else {
            this.pstSelectByExample.setString(4, "%");

        }


        ResultSet rs = pstSelectByExample.executeQuery();
        while (rs.next()) {
            listaArticulos.add(new Articulo(rs.getInt("id"), rs.getString("nombre"), rs.getFloat("precio"), rs.getString("codigo"), grDAO.findByPK(rs.getInt("grupo"))));
        }
        rs.close();
        return listaArticulos;
    }


    @Override
    public boolean insert(Articulo arti) throws SQLException {
        pstInsert.setString(1, arti.getNombre());
        pstInsert.setFloat(2, arti.getPrecio());
        pstInsert.setString(3, arti.getCodigo());
        pstInsert.setInt(4, arti.getGrupo().getId());
        pstInsert.registerOutParameter(5, Types.INTEGER);
        int insertados = pstInsert.executeUpdate();
        return (insertados == 1);
    }

    public void insertLote(Articulo arti) throws SQLException {
        pstInst.setString(1, arti.getNombre());
        pstInst.setFloat(2, arti.getPrecio());
        pstInst.setString(3, arti.getCodigo());
        pstInst.setInt(4, arti.getGrupo().getId());
        pstInst.addBatch();

    }

    public int[] ejecutarbatch() throws SQLException {
        int[] r = new int[0];
        int[] result = null;
        try {
            result = pstInst.executeBatch();
           // System.out.println("Resutado: " + Arrays.toString(result));
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Lista de peradcion exitosa");
            alert.setHeaderText("Resultado: "+Arrays.toString(result));
            alert.showAndWait();
            con.commit();
        } catch (
                BatchUpdateException ex) {
           vista.Util.mensajeExcepcion(ex,"Error Batch");
            //System.err.println("Error Batch "+ex.getMessage());
            //RESULTADO
             r = ex.getUpdateCounts();
            System.out.println(Arrays.toString(r));
            try {
                con.commit();

            } catch (SQLException ex1) {
                vista.Util.mensajeExcepcion(ex1,"error haciendo rollback");
              // System.out.println("error haciendo rollback");
            }

        } catch (SQLException ex) {
            System.err.println("Error sql");
            try {
                con.commit();

            } catch (SQLException ex1) {
                vista.Util.mensajeExcepcion(ex1,"error haciendo rollback");
               // System.out.println("error haciendo rollback");
            }
        } finally {
            System.out.println("Fin lote");
            try {
                // Hace commit automaticamente
                con.setAutoCommit(true);
            } catch (SQLException ex) {
                vista.Util.mensajeExcepcion(ex,"error haciendo rollback");
               // System.out.println("error poniendo autocommit a true");
            }
        }
        return r;
    }

    @Override
    public Articulo insertGenKey(Articulo arti) throws SQLException {
        Articulo artic = null;

        pstInsertGenKey.setString(1, arti.getNombre());
        pstInsertGenKey.setFloat(2, arti.getPrecio());
        pstInsertGenKey.setString(3, arti.getCodigo());
        pstInsertGenKey.setInt(4, arti.getGrupo().getId());
        pstInsertGenKey.registerOutParameter(5, Types.INTEGER);
        int insertados = pstInsertGenKey.executeUpdate();
        if (insertados == 1) {


            artic = new Articulo(pstInsertGenKey.getInt(5), arti.getNombre(), arti.getPrecio(), arti.getCodigo(), arti.getGrupo());
            return artic;
        }
        return artic;
    }

    @Override
    public boolean update(Articulo arti) throws SQLException {
        pstUpdate.setString(1, arti.getNombre());
        pstUpdate.setFloat(2, arti.getPrecio());
        pstUpdate.setString(3, arti.getCodigo());
        pstUpdate.setInt(4, arti.getGrupo().getId());
        pstUpdate.setInt(5, arti.getId());
        int actualizados = pstUpdate.executeUpdate();
        return (actualizados == 1);
    }

    @Override
    public boolean delete(int id) throws SQLException {
        pstDelete.setInt(1, id);
        int borrados = pstDelete.executeUpdate();
        return (borrados == 1);
    }

    @Override
    public boolean delete(Articulo arti) throws SQLException {
        return this.delete(arti.getId());
    }

}
